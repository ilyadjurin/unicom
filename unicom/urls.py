from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from django.views.generic import RedirectView
from rest_framework.routers import DefaultRouter
from rest_framework_swagger.views import get_swagger_view

from apps.core.api.views import ApplicationListViewSet

schema_view = get_swagger_view(title='Unicom API')

router = DefaultRouter()
router.register('application', ApplicationListViewSet)

urlpatterns = [
    path('', RedirectView.as_view(url='/admin')),
    path('apidocs/', schema_view),
    path('admin/', admin.site.urls),
    path('api/v1/partner/', include('apps.core.api.urls')),
    path('api/v1/credit/', include(router.urls),)
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) \
                   + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
