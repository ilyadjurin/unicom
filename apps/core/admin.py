from django.contrib import admin
from django.contrib.auth.models import Group

from apps.core.models import Proposal, Questionnaire, Application


@admin.register(Proposal)
class ProposalAdmin(admin.ModelAdmin):
    model = Proposal

    list_display = ('name', 'type_proposal', 'enterprise')
    fields = ('rotations_start', 'rotations_end', 'name', 'type_proposal', 'scoring_min', 'scoring_max', 'enterprise')
    raw_id_fields = ('enterprise',)
    list_filter = ('name', 'type_proposal', 'enterprise')
    search_fields = ('name', 'scoring_min', 'scoring_max')


@admin.register(Questionnaire)
class QuestionnaireAdmin(admin.ModelAdmin):
    model = Questionnaire

    list_display = ('name', 'patronymic', 'date_birth', 'phone_number', 'partner')
    fields = (
        'name', 'surname', 'patronymic', 'date_birth', 'phone_number', 'passport_number', 'scoring_score', 'partner')
    readonly_fields = ()
    raw_id_fields = ('partner',)
    list_filter = ('name', 'surname', 'patronymic', 'partner')
    search_fields = ('name', 'surname', 'patronymic')

    def get_readonly_fields(self, request, obj=None):
        if Group.objects.filter(user=request.user,
                                name='partners').exists() and request.path_info != '/admin/core/questionnaire/add/':
            self.readonly_fields = (
                'name', 'surname', 'patronymic', 'date_birth', 'phone_number', 'passport_number', 'scoring_score',
                'partner')
        return self.readonly_fields

    def get_queryset(self, request):
        qs = self.model._default_manager.get_queryset()
        ordering = self.get_ordering(request)
        if ordering:
            qs = qs.order_by(*ordering)
        if Group.objects.filter(user=request.user, name='partners').exists():
            qs = qs.filter(partner=request.user)
        return qs


@admin.register(Application)
class ApplicationAdmin(admin.ModelAdmin):
    model = Application

    list_display = ('id', 'created_at', 'application_state')
    fields = ('created_at', 'departure_date', 'questionnaire', 'proposal', 'application_state')
    raw_id_fields = ('questionnaire', 'proposal')
    readonly_fields = ()
    list_filter = ('id', 'created_at')
    search_fields = ('id', 'application_state')

    def get_readonly_fields(self, request, obj=None):
        if Group.objects.filter(user=request.user, name='credit_institution').exists():
            self.readonly_fields = ('created_at', 'departure_date', 'questionnaire', 'proposal')
        elif Group.objects.filter(user=request.user, name='partners').exists():
            self.readonly_fields = ('created_at', 'departure_date', 'questionnaire', 'proposal', 'application_state')
        return self.readonly_fields

    def get_queryset(self, request):
        qs = self.model._default_manager.get_queryset()
        ordering = self.get_ordering(request)
        if ordering:
            qs = qs.order_by(*ordering)

        if Group.objects.filter(user=request.user, name='credit_institution').exists():
            qs = qs.filter(application_state__in=[Application.SHIPPED])
        elif Group.objects.filter(user=request.user, name='partners').exists():
            qs = qs.filter(
                application_state__in=[Application.NEW, Application.RECEIVED, Application.APPROVED, Application.DENIED,
                                       Application.ISSUED])
        return qs
