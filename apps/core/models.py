from django.core.validators import RegexValidator
from django.db import models

from apps.users.models import Client


class Proposal(models.Model):
    LOAN, MORTGAGE, AUTOLOAN = range(3)

    PROPOSAL_TYPE = (
        (LOAN, 'consumer loan'),
        (MORTGAGE, 'mortgage'),
        (AUTOLOAN, 'loan for auto'),
    )

    updated_at = models.DateTimeField(auto_now=True, verbose_name="updated timestamp")
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="creation timestamp")
    rotations_start = models.DateTimeField(verbose_name="start rotations")
    rotations_end = models.DateTimeField(verbose_name="end rotations")
    name = models.CharField(max_length=256, verbose_name="proposal name")
    type_proposal = models.PositiveSmallIntegerField(choices=PROPOSAL_TYPE, default=LOAN,
                                                     verbose_name="proposal type")
    scoring_min = models.PositiveSmallIntegerField(verbose_name="minimum scoring score")
    scoring_max = models.PositiveSmallIntegerField(verbose_name="maximum scoring score")
    enterprise = models.ForeignKey(Client, on_delete=models.CASCADE, verbose_name='credit institution',
                                   related_name='proposals')

    class Meta:
        verbose_name = "proposal"
        verbose_name_plural = "proposals"

    def __str__(self):
        return '%s' % (self.name,)


class Questionnaire(models.Model):
    updated_at = models.DateTimeField(auto_now=True, verbose_name="updated timestamp")
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="creation timestamp")
    name = models.CharField(max_length=64)
    surname = models.CharField(max_length=64)
    patronymic = models.CharField(max_length=64)
    date_birth = models.DateTimeField(verbose_name="date of birth")
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Phone number must be entered in the format: '+777777777777'."
                                         " Up to 15 digits allowed.")
    phone_number = models.CharField(validators=[phone_regex], max_length=17, verbose_name="phone number")
    passport_number = models.CharField(max_length=128, verbose_name="passport number")
    scoring_score = models.PositiveSmallIntegerField(verbose_name=" scoring score")
    partner = models.ForeignKey(Client, on_delete=models.CASCADE, verbose_name='partner',
                                related_name='questionnaires')

    class Meta:
        verbose_name = "questionnaire"
        verbose_name_plural = "questionnaires"

    def __str__(self):
        return '%s' % (self.name,)


class Application(models.Model):
    NEW, SHIPPED, RECEIVED, APPROVED, DENIED, ISSUED = range(6)

    STATE_APPLICATION = (
        (NEW, 'new'),
        (SHIPPED, 'shipped'),
        (RECEIVED, 'received'),
        (APPROVED, 'approved'),
        (DENIED, 'denied'),
        (ISSUED, 'issued'),
    )

    created_at = models.DateTimeField(verbose_name="creation date")
    departure_date = models.DateTimeField(verbose_name="departure date", blank=True, null=True)
    questionnaire = models.ForeignKey(Questionnaire, on_delete=models.CASCADE, verbose_name='questionnaire',
                                      related_name='applications')
    proposal = models.ForeignKey(Proposal, on_delete=models.CASCADE, verbose_name='proposal',
                                 related_name='applications')
    application_state = models.PositiveSmallIntegerField(choices=STATE_APPLICATION, default=NEW,
                                                         verbose_name="application state")

    class Meta:
        verbose_name = "application"
        verbose_name_plural = "applications"

    def __str__(self):
        return '%s' % (self.id)
