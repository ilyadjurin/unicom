from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.relations import PrimaryKeyRelatedField

from apps.core.models import Questionnaire, Proposal, Application


class QuestionnaireListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Questionnaire
        fields = '__all__'


class ProposalListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Proposal
        fields = '__all__'


class ApplicationListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Application
        fields = '__all__'


class SendApplicationSerializer(serializers.Serializer):
    credit = PrimaryKeyRelatedField(queryset=Application.objects.all(), required=True)
    application_state = serializers.ChoiceField(choices=Application.STATE_APPLICATION, required=True)

    def validate(self, attrs):
        if not Application.objects.filter(id=attrs['application_state']).exists():
            raise ValidationError("Application does not exists")
        elif not attrs['credit'].id in range(6):
            raise ValidationError("incorrect state")
        attrs['app'] = Application.objects.filter(id=attrs['application_state']).last()
        return super(SendApplicationSerializer, self).validate(attrs)
