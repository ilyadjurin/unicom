from django.contrib.auth.models import Group
from rest_framework.test import APITestCase, APIRequestFactory, force_authenticate

from apps.core.api.views import QuestionnaireListViewSet, ProposalListViewSet, SendiApplicationView, \
    ApplicationListViewSet
from apps.core.models import Application, Questionnaire, Proposal
from apps.users.models import Client


class TestCoreApi(APITestCase):
    groups = [
        'partners',
        'credit_institution'
    ]

    def setUp(self):
        super().setUp()
        self.customer_partner, _ = Client.objects.get_or_create(
            username='partner'
        )
        self.customer_credit, _ = Client.objects.get_or_create(
            username='credit'
        )
        for name in self.groups:
            self.group, _ = Group.objects.get_or_create(name=name)
            if name == 'partners':
                self.group.user_set.add(self.customer_partner)
            else:
                self.group.user_set.add(self.customer_credit)

    def test_customer_api(self):

        factory = APIRequestFactory()

        request = factory.post('/partner/questionnaire/', data={
            "updated_at": "2019-04-04T18:04:36.089910+03:00",
            "created_at": "2019-04-04T18:04:36.090012+03:00",
            "name": "ilya",
            "surname": "djurin",
            "patronymic": "alex",
            "date_birth": "1993-04-04T19:04:08+04:00",
            "phone_number": "+38066886110",
            "passport_number": "qw11457678",
            "scoring_score": 1,
            "partner": 1})
        force_authenticate(request, user=self.customer_partner)
        view = QuestionnaireListViewSet.as_view({'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, 201)

        request = factory.get('/partner/questionnaire/')
        force_authenticate(request, user=self.customer_partner)
        view = QuestionnaireListViewSet.as_view({'get': 'list'})
        response = view(request)
        self.assertEqual(response.status_code, 200)

        request = factory.put('/partner/questionnaire/')
        force_authenticate(request, user=self.customer_partner)
        view = QuestionnaireListViewSet.as_view({'put': 'retrieve'})
        response = view(request, pk=1)
        self.assertEqual(response.status_code, 200)

        request = factory.get('/partner/proposal/')
        force_authenticate(request, user=self.customer_partner)
        view = ProposalListViewSet.as_view()
        response = view(request)
        self.assertEqual(response.status_code, 200)

        self.application = Application.objects.create(created_at="2019-04-04T20:26:46+03:00",
                                                      departure_date="2019-04-04T20:26:47+03:00",
                                                      application_state=1,
                                                      questionnaire=Questionnaire.objects.first(),
                                                      proposal=Proposal.objects.create(
                                                          updated_at="2019-04-04T17:33:22.096741+03:00",
                                                          created_at="2019-04-04T17:33:22.096835+03:00",
                                                          rotations_start="2019-04-04T17:32:10+03:00",
                                                          rotations_end="2019-04-04T17:32:11+03:00",
                                                          name="first prop",
                                                          type_proposal=0,
                                                          scoring_min=1,
                                                          scoring_max=100,
                                                          enterprise=self.customer_partner))

        request = factory.post('/partner/questionnaire/', data={"credit": "1", "application_state": 1})
        force_authenticate(request, user=self.customer_partner)
        view = SendiApplicationView.as_view()
        response = view(request)
        self.assertEqual(response.status_code, 201)

        request = factory.get('/credit/application/')
        force_authenticate(request, user=self.customer_credit)
        view = ApplicationListViewSet.as_view({'get': 'list'})
        response = view(request)
        self.assertEqual(response.status_code, 200)

        request = factory.put('/credit/application/')
        force_authenticate(request, user=self.customer_credit)
        view = ApplicationListViewSet.as_view({'put': 'retrieve'})
        response = view(request, pk=1)
        self.assertEqual(response.status_code, 200)
