from django.db import migrations

groups = [
    'partners',
    'credit_institution'
]


def defaut_groups(apps, schema_editor):
    db_alias = schema_editor.connection.alias
    Group = apps.get_model('auth', 'Group')

    for name in groups:
        grp, res = Group.objects.using(db_alias).get_or_create(name=name)


class Migration(migrations.Migration):
    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(defaut_groups, migrations.RunPython.noop),
    ]
