from django.contrib.auth.models import Group
from rest_framework.permissions import BasePermission


class IsPartnerGroup(BasePermission):

    message = 'you do not have permission to view(user must belong to only one group(partners or credit_institution ))'

    def has_permission(self, request, view):
        if request.user.is_superuser:
            return True
        if request.user.groups.exists():
            if len(request.user.groups.all()) > 1:
                return False
            if request.user.groups.last() == Group.objects.get(name='partners'):
                return True
        return False


class IsCreditGroup(BasePermission):

    message = 'you do not have permission to view(user must belong to only one group(partners or credit_institution ))'

    def has_permission(self, request, view):
        if request.user.is_superuser:
            return True
        if request.user.groups.exists():
            if len(request.user.groups.all()) > 1:
                return False
            if request.user.groups.last() == Group.objects.get(name='credit_institution'):
                return True
        return False
