from rest_framework import status, filters, mixins
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet, ReadOnlyModelViewSet

from apps.core.api.permissions import IsPartnerGroup, IsCreditGroup
from apps.core.models import Questionnaire, Proposal, Application
from .serializers import QuestionnaireListSerializer, ProposalListSerializer, SendApplicationSerializer, \
    ApplicationListSerializer


############################# API for partners #############################

class QuestionnaireListViewSet(mixins.RetrieveModelMixin, mixins.ListModelMixin, mixins.CreateModelMixin,
                               GenericViewSet):
    """
    List/Retrieve/Create questionnaire
    """
    permission_classes = (IsAuthenticated, IsPartnerGroup)

    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    search_fields = ('name', 'surname', 'patronymic')
    ordering_fields = ('updated_at', 'created_at', 'id', 'date_birth', 'scoring_score')

    queryset = Questionnaire.objects.all()
    serializer_class = QuestionnaireListSerializer


class ProposalListViewSet(ListAPIView):
    """
    List Proposal
    """
    permission_classes = (IsAuthenticated, IsPartnerGroup)
    queryset = Proposal.objects.all()
    serializer_class = ProposalListSerializer


class SendiApplicationView(APIView):
    """
    Отправка заявки в кредитные организации
    Отправка заявки представляет собой изменение статуса заявки, соотвественно области видимости для кредитора и партнера
    """
    permission_classes = (IsAuthenticated, IsPartnerGroup)
    serializer_class = SendApplicationSerializer

    def post(self, request):
        serializer = SendApplicationSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        application = serializer.validated_data['app']
        application.application_state = serializer.validated_data['application_state']
        application.save(update_fields=['application_state'])
        return Response({'status': 'success shipped'}, status=status.HTTP_201_CREATED)


############################# API for credit institutions #############################


class ApplicationListViewSet(ReadOnlyModelViewSet):
    """
    List/Retrieve application
    """
    permission_classes = (IsAuthenticated, IsCreditGroup)

    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    search_fields = ('id', 'application_state')
    ordering_fields = ('created_at', 'departure_date', 'id')

    queryset = Application.objects.all()
    serializer_class = ApplicationListSerializer
