from django.urls import path, include
from rest_framework.routers import DefaultRouter

from apps.core.api.views import QuestionnaireListViewSet, ProposalListViewSet, \
    SendiApplicationView

router = DefaultRouter()
router.register('questionnaire', QuestionnaireListViewSet)
# router.register('qs', QuestionnaireCreateRetrieveViewSet)

urlpatterns = [
    path('', include(router.urls), ),
    path('proposal/', ProposalListViewSet.as_view()),
    path('send_app/', SendiApplicationView.as_view()),
]
