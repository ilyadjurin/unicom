from django.contrib.admin import site
from django.contrib.auth.admin import UserAdmin

from .models import Client

site.register(Client, UserAdmin)

